"use strict";
try {

  const ctxt_pos = 0,
    ctxt_title = 1,
    ctxt_modal = 2,
    ctxt_prodlist = 2,
    ctxt_bg = 4,
    ctxt_logo = 5;

  var sto = window.__sto,
    settings = require("./../../settings.json"),
    $ = window.jQuery,
    html = require("./main.html"),
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    style = require("./main.css"),
    container = $(html),
    creaid = settings.creaid,
    format = settings.format,
    custom = settings.custom,
    products = [],
    btfpos = settings.btfpos !== "mid" && settings.btfpos !== "top" ? "top" : settings.btfpos,
    container = $(html),
    helper_methods = sto.utils.retailerMethod,
    redirect_url = settings.redirect,
    btnSize = settings.btnSize,
    left_txt = settings.left_text,
    sto_global_nbproducts = 0,
    sto_global_products = [],
    sto_global_modal,
    promise,
    pdf,
    pos, posGlobals;

  var fontRoboto = $('<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
  $('head').first().append(fontRoboto);

  module.exports = {
    init: _init_()
  };

  function _init_() {

    sto.load([format, creaid], function(tracker) {



      var removers = {};
      removers[format] = {
        "creaid": creaid,
        "func": function() {
          style.unuse();
          container.remove(); //verifier containerFormat j'ai fait un copier coller
        }
      };

      var int, isElementInViewport;

      isElementInViewport = function(el) {

        if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
          el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
          rect.top >= 0 &&
          rect.left >= 0 &&
          !(rect.top == rect.bottom || rect.left == rect.right) &&
          !(rect.height == 0 || rect.width == 0) &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
          rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );

      }

      if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 3){
        return removers;
      }

      // if (window.matchMedia('(min-width: 786px)').matches) {
      //   tracker.error({
      //     "tl": "onlyOnMobile",
      //   });
      //   return removers;
      //   return false;
      // }

      /*if ($('div[data-pos="' + settings.btfpos + '"]').length > 0) {
        tracker.error({
          "tl": "placementOcuppied"
        });
        return removers;
        return false;
      }*/

      if ($('div[data-pos="13"]').length > 0 && $('div[data-pos="25"]').length > 0) {
        tracker.error({
          "tl": "placementOcuppied"
        });
        return removers;
        return false;
      }

      helper_methods.crawl(settings.crawl).promise.then(function(d) {

        try {

          // if ($('div[data-pos="13"]').length <= 0 && $('div[data-pos="25"]').length <= 0) {
          //   container.attr('data-pos', '13');
          // } else if ($('div[data-pos="13"]').length > 0 && $('div[data-pos="25"]').length <= 0) {
          //   container.attr('data-pos', '25');
          // }

          var prodsInpage = $('#productList>li').length;

          if (prodsInpage >= 14 && prodsInpage <= 20 && $('.sto-' + format).length < 1) {
            helper_methods.createFormat(container, tracker, settings.custom[0][2], d);
          } else if (prodsInpage > 20 && $('.sto-' + format).length < 4) {
            helper_methods.createFormat(container, tracker, settings.custom[0][2], d);
          } else {
            tracker.error({
              "tl": "notEnoughProdsInPage"
            });
          }

          style.use();

          // <editor-fold> INSERT FORMAT ********************************************************

          //insert format
          var insertFormat = function() {

            var widthTile = $('#productList>li').width();
            var widthLine = $('.product-list').width();
            var nbTile;

            if (widthTile * 2 > widthLine) {
              nbTile = 1;
            } else if (widthTile * 3 > widthLine) {
              nbTile = 2;
            } else {
              nbTile = 3;
            }

            var sponso = $('.hl-beacon-universal').length;
            if (sponso >= 1) {
              sponso = 1;
            }

            var sponsoTech = $('.breakzone').length;
            if (sponsoTech >= 1) {
              sponsoTech = 1;
            }
            // if ($('.sto-format').length == 0 && sponsoTech >= 1 && nbTile == 1) {
            //   sponsoTech = 5;
            // } else if ($('.sto-format').length == 1 && sponsoTech >= 1 && nbTile == 1) {
            //   sponsoTech = 1;
            // } else if ($('.sto-format').length == 2 && sponsoTech >= 1 && nbTile == 1) {
            //   sponsoTech = -3;
            // } else {
            //   sponsoTech = 0;
            // }

            //var $('.sto-format:not(.sto-AMSK, .sto-AFOO)').length = $('.sto-format').length - $('.sto-AMSK').length;

            if (window.matchMedia("(max-width: 767px)").matches && nbTile == 1) {
                if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                  $('#productList>li:visible').eq( nbTile * 5).before(container);
                  container.attr('data-pos', 6);
                  posGlobals = 6;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                  $('#productList>li:visible').eq( nbTile * 14).before(container);
                  container.attr('data-pos', 16);
                  posGlobals = 16;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                  $('#productList>li:visible').eq( nbTile * 23).before(container);
                  container.attr('data-pos', 26);
                  posGlobals = 26
              }
            } else if (window.matchMedia("(min-width: 768px)").matches && nbTile == 1) {
                if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                  $('#productList>li:visible').eq( nbTile * (11 + sponsoTech)).before(container);
                  container.attr('data-pos', 12 + sponsoTech);
                  posGlobals = 12;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                  $('#productList>li:visible').eq( nbTile * (16 + sponsoTech)).before(container);
                  container.attr('data-pos', 18 + sponsoTech);
                  posGlobals = 18;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                  $('#productList>li:visible').eq( nbTile * (21 + sponsoTech)).before(container);
                  container.attr('data-pos', 24 + sponsoTech);
                  posGlobals = 24
              }
            } else {
                if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                  $('#productList>li:visible').eq( nbTile * (5 + sponso)).before(container);
                  container.attr('data-pos', 6 + sponso);
                  posGlobals = 6;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                  $('#productList>li:visible').eq( nbTile * (14 + sponso)).before(container);
                  container.attr('data-pos', 16 + sponso);
                  posGlobals = 16;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                  $('#productList>li:visible').eq( nbTile * (23 + sponso)).before(container);
                  container.attr('data-pos', 26 + sponso);
                  posGlobals = 26
                }
              }
            };

          insertFormat();

                pos = $('.sto-' + placeholder + '-w-butterfly').attr('data-pos');


          // </editor-fold> *********************************************************************


          // <editor-fold> LEFT *****************************************************************

          // TEXTE
          if (left_txt && left_txt != "") {
            $(container).find('.sto-' + placeholder + '-left .sto-' + placeholder + '-left-content').prepend($("<p class='sto-" + placeholder + "-left-txt'>" + left_txt + "</p>"));
          }

          $(container).find('.sto-' + placeholder + '-left .sto-' + placeholder + '-left-content').append($("<div class='sto-" + placeholder + "-left-arrow'></div>"));


          // </editor-fold> *********************************************************************


          // for (var i = 0; i < 1; i++) {
          //   browseObj(i, d, tracker);
          // }


          // <editor-fold> availability ****************************************************************************
          function verifAvailability() {

            // console.log('hello');


            var productList = []

            for (var q = 0; q < custom.length; q++) {
              productList.push(custom[q][ctxt_prodlist][0]);
            }

            for (var i = 0; i <= productList.length; i++) {
              var value = productList[i];
              if (d[value] !== undefined && d[value].dispo !== false) {
                sto_global_products.push(value);
                sto_global_nbproducts += 1;
                return value;
              }
            }
            return false;
          }

          var available = verifAvailability();

          if (available =! false) {
            tracker.availability({
              "po": pos,
              "tn": sto_global_nbproducts
            });
          }
          // </editor-fold> ****************************************************************************************

          

          if ($('.sto-' + placeholder + '-w-butterfly').length === 1) {
            tracker.display({
              "po": pos
            });
          }


          var trackingView = false;
          var windowCenterTop = $(window).height() / 4;
          var windowCenterBot = $(window).height() - windowCenterTop;
          var eTop = $('.sto-' + placeholder + '-w-butterfly').offset().top;

          var dataPosition = $('.sto-' + placeholder + '-w-butterfly').attr('data-pos');

          // console.log(dataPosition);
          var distanceToTop;
          var trackScroll = function() {
            distanceToTop = eTop - $(window).scrollTop();
            if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
              tracker.view({
                // "tl": dataPosition
                "po": pos
              });
              trackingView = true;
            }
          };
          // trackScroll();

          //
          // $(window).scroll(function() {
          //   trackScroll();
          // });

          $('.sto-' + placeholder + '-w-butterfly').attr('data-format-type', settings.format);
          $('.sto-' + placeholder + '-w-butterfly').attr('data-crea-id', settings.creaid);

          if (settings.text_cta != "") {
            $(".sto-" + placeholder + "-cta").append('<span>' + settings.text_cta + '</span>');
          }
          if (settings.claim_text != "") {
            $(".sto-" + placeholder + "-claim").append('<span>' + settings.claim_text + '</span>');
          }

          if (settings.redirect != "") {
            $(document).on('click', ".sto-" + placeholder + "-cta", function() {
              try {
                tracker.click({
                  "tl": "cta",
                  "po": pos
                });
              } catch (e) {
                console.log(e)
              }
              window.open(settings.redirect, settings.target);
            });
          }

          var prodUrl = $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass > a').attr('href');
          $(document).on('click', '.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-vignette', function() {
            tracker.moreInfo({
              "po": pos
            });
            window.open(prodUrl, '_self');
          });




          var prodName = settings.custom[0][1];
          var prodPriceOld = $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-original-price') ? $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-original-price').text() : "";

          var prodPromo = $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.thumbnail span.pl-promo') ? $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.thumbnail span.pl-promo').text() : "";

          var prodPriceFinal = $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-reduced-price') ? $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-reduced-price').text() : "";

          if ($('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-eco-part').length > 0) {
            var priceEco = $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-eco-part').text();
          }else{
            var priceEco = '';
          }

          if ($('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-reduced-price').length > 0) {
            $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-eco-part').remove();
            var prodPriceFinal = $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-reduced-price').text();
          } else if ($('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-price').length > 0) {
            $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-eco-part').remove();
            var prodPriceFinal = $('.sto-' + placeholder + '-w-butterfly .sto-product-container> .sto-product-klass').eq(0).find('.pl-price-line .pl-price').text();
          } else {
            var prodPriceFinal = "";
          }



          if (settings.custom[0][3] != null && settings.custom[0][3] != "None") {
            var prodRatings = settings.custom[0][3];
            prodRatings = prodRatings.replace('.', '_');
          }


          var prodRatingsDom = $('<span class="product-rating-stars"> <span class="disable-stars"> <span class="stars icon icon-star-disable"></span><span class="stars icon icon-star-disable"></span><span class="stars icon icon-star-disable"></span><span class="stars icon icon-star-disable"></span><span class="stars icon icon-star-disable"></span> </span> <span class="enable-stars rating-' + prodRatings + '"> <span class="stars icon icon-star-enable"></span><span class="stars icon icon-star-enable"></span><span class="stars icon icon-star-enable"></span><span class="stars icon icon-star-enable"></span><span class="stars icon icon-star-enable"></span> </span> </span>');



          var prodComment = settings.custom[0][4];
          $('.sto-' + placeholder + '-content').append('<div class="sto-' + placeholder + '-w-infos"></div>');

          $('.sto-' + placeholder + '-w-infos').append('<div class="sto-' + placeholder + '-infos-title">' + prodName + '</div><div class="sto-' + placeholder + '-infos-prices"><span class="sto-reduction">' + prodPromo + '</span> ' + prodPriceFinal + ' <span class="sto-old-price">' + prodPriceOld + '</span><span class="sto-eco-price">' + priceEco + '</span></div>');

          if (settings.custom[0][3] != null && settings.custom[0][3] != "None") {
            $('.sto-' + placeholder + '-w-butterfly').attr('data-type', 'ratings');
            $('.sto-' + placeholder + '-w-infos').append('<div class="sto-' + placeholder + '-infos-ratings"></div>');
            $('.sto-' + placeholder + '-infos-ratings').prepend(prodRatingsDom);
          }

          if (prodComment != "" && settings.custom[0][3] != null && settings.custom[0][3] != "None") {
            $('.sto-' + placeholder + '-w-infos').append('<div class="sto-' + placeholder + '-infos-comments">\"' + prodComment + '\"</div>');
          } else {
            $('.sto-' + placeholder + '-w-butterfly').attr('data-type', 'name');
          }

          /*if (settings.custom[0][3] != null && prodComment != "") {

          } else {
            $('.sto-' + placeholder + '-w-butterfly').attr('data-type', 'name');
          }*/

          //center content
          var centerContent = function() {
            var logoH = $(".sto-" + placeholder + "-w-logo").height();
            var contentH = $(".sto-" + placeholder + "-content").height();
            var selectionH = $(".sto-" + placeholder + "-selection").height();
            if ($(".sto-" + placeholder + "-w-cta")) {
              var recetteH = $(".sto-" + placeholder + "-w-cta").height();
            } else {
              var recetteH = 0;
            }
            var contentPos = (selectionH - (contentH + recetteH + logoH)) / 2;
            $(".sto-" + placeholder + "-content").css('margin-top', contentPos + 'px');
          };

          var btffHomote;
          window.addEventListener("resize", function() {

            var resizeFormat = function() {
              var widthTile = $('#productList>li').width();
              var widthLine = $('.product-list').width();
              var nbTile;

              if (widthTile * 2 > widthLine) {
                nbTile = 1;
              } else if (widthTile * 3 > widthLine) {
                nbTile = 2;
              } else {
                nbTile = 3;
              }

              $('.sto-format').each(function() {
                var dataPos = $(this).attr('data-pos');

                // if (window.matchMedia("(max-width: 767px)").matches) {

                if (nbTile == 1) {
                  if (window.matchMedia("(max-width: 767px)").matches) {
                    if (dataPos >= 6 && dataPos <= 13) {
                      $('#productList>li:visible').eq( nbTile * 5).before(this);
                    } else if (dataPos >= 16 && dataPos <= 20) {
                      $('#productList>li:visible').eq( nbTile * 14).before(this);
                    } else if (dataPos >= 22 && dataPos <= 28) {
                      $('#productList>li:visible').eq( nbTile * 23).before(this);
                    }
                  } if (window.matchMedia("(min-width: 768px)").matches) {
                    if (dataPos >= 6 && dataPos <= 13) {
                      $('#productList>li:visible').eq( nbTile * (dataPos - 1)).before(this);
                    } else if (dataPos >= 16 && dataPos <= 20) {
                      $('#productList>li:visible').eq( nbTile * (dataPos - 2)).before(this);
                    } else if (dataPos >= 22 && dataPos <= 28) {
                      $('#productList>li:visible').eq( nbTile * (dataPos - 3)).before(this);
                    }
                  }
                } else if (nbTile > 1) {
                  if (dataPos >= 6 && dataPos <= 13) {
                    $('#productList>li:visible').eq( nbTile * (dataPos - 1)).before(this);
                  } else if (dataPos >= 16 && dataPos <= 17) {
                    $('#productList>li:visible').eq( nbTile * (dataPos - 2)).before(this);
                  } else if (dataPos >= 23 && dataPos <= 27) {
                    $('#productList>li:visible').eq( nbTile * (dataPos - 3)).before(this);
                  }
                }
              });
            }
            resizeFormat();

            //BTF height homotetique
            $('.sto-' + placeholder + '-w-butterfly').css('height', ((77.3 * $('.sto-' + placeholder + '-w-butterfly').width()) / 100) + 'px');
            //Logo height homotetique
            $('.sto-' + placeholder + '-cta,  .sto-' + placeholder + '-logo').css('height', ((25 * $('.sto-' + placeholder + '-logo').width()) / 100) + 'px');

            centerContent();

          });
          window.setTimeout(function() {
            //window.dispatchEvent(new window.Event("resize"));
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
          });

          // console.log(posGlobals);

          // STORE FORMAT FOR FILTERS
          sto.globals["print_creatives"].push({
            html: container,
            parent_container_id: "#productList",
            type: settings.format,
            crea_id: settings.creaid,
            position: posGlobals
          });



        } catch (e) {
          console.log(e);
          style.unuse();
          $(".sto-" + placeholder + "-container").remove();
          tracker.error({
            "te": "onBuild-format",
            "tl": e,
            "po": pos
          });
        }
      }).then(null, console.log.bind(console));


      return removers;
    });

  }

  // function verifAvailability(a, d) {
  //
  //   var l = a[ctxt_prodlist].length;
  //   for (var i = 0; i <= (l - 1); i++) {
  //     var value = a[ctxt_prodlist][i];
  //     if (d[value] !== undefined && d[value].dispo !== false) {
  //       sto_global_products.push(value);
  //       sto_global_nbproducts += 1;
  //
  //       tracker.availability({
  //         "po": pos
  //       });
  //
  //       return value;
  //
  //     }
  //   }
  //   return false;
  // }



  function addButton(cug, title, index) {
    $('.sto-' + placeholder + '-w-buttons').append('<button id="' + cug + '" data-index="' + (index + 1) + '">' + title + '</button>');
  }

  function browseObj(index, d, tracker) {
    var currentbObj = custom[index];
    if (currentbObj) {
      var title = currentbObj[ctxt_title],
        modal = currentbObj[ctxt_modal],
        available = verifAvailability(currentbObj, d);
        debugger;
      modal = modal === "true" ? true : false;

      if (available != false) {

        if (modal) {
          sto_global_modal = true;
        }
        addButton(available, title, index, tracker);
      }
    }

  }

  function firstDisplay() {
    $('.sto-' + placeholder + '-w-butterfly .sto-product-container .vignette_produit_info:first-child').show();
    $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).addClass('sto-' + placeholder + '-selButton');
    if (settings.custom_images === true) {
      $('.sto-' + placeholder + '-w-butterfly').attr('data-index', $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).attr('data-index'));
    }
  }

  function selectProduct(index) {


    $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button').removeClass('sto-' + placeholder + '-selButton');
    $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button[data-index="' + index + '"]').addClass('sto-' + placeholder + '-selButton');

    index = parseInt(index) - 1;

    $('.sto-' + placeholder + '-w-vignette').attr('data-ind', index);
    $('.sto-' + placeholder + '-w-vignette').attr('data-href', settings.custom[index][3]);

  }

} catch (e) {

}
