"use strict";

try {

  const ctxt_pos = 0,
    ctxt_title = 1,
    ctxt_modal = 2,
    ctxt_prodlist = 2,
    ctxt_bg = 4,
    ctxt_logo = 5;

  var sto = window.__sto,
    settings = require("./../../settings.json"),
    $ = window.jQuery,
    html = require("./main.html"),
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    style = require("./main.css"),
    container = $(html),
    creaid = settings.creaid,
    format = settings.format,
    custom = settings.custom,
    products = [],
    btfpos = settings.btfpos !== "mid" && settings.btfpos !== "top" ? "top" : settings.btfpos,
    container = $(html),
    helper_methods = sto.utils.retailerMethod,
    redirect_url = settings.redirect,
    btnSize = settings.btnSize,
    crawl = settings.crawl.replace("https://drive.intermarche.com/", ""),
    myurl = window.location.href.replace("https://drive.intermarche.com/", ""),
    mydrive = myurl.split("/")[0],
    left_txt = settings.left_text,
    sto_global_nbproducts = 0,
    sto_global_products = [],
    sto_global_modal,
    promise,
    pdf,
    pos;

  var fontRoboto = $('<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
  $('head').first().append(fontRoboto);

  module.exports = {
    init: _init_()
  };

  function _init_() {

    sto.load([format, creaid], function(tracker) {



      var removers = {};
      removers[format] = {
        "creaid": creaid,
        "func": function() {
          style.unuse();
          container.remove(); //verifier containerFormat j'ai fait un copier coller
        }
      };

      var int, isElementInViewport;

      isElementInViewport = function(el) {

        if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
          el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
          rect.top >= 0 &&
          rect.left >= 0 &&
          !(rect.top == rect.bottom || rect.left == rect.right) &&
          !(rect.height == 0 || rect.width == 0) &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
          rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );

      }

      // if (window.matchMedia('(min-width: 786px)').matches) {
      //   tracker.error({
      //     "tl": "onlyOnMobile",
      //   });
      //   return removers;
      //   return false;
      // }

      /*if ($('div[data-pos="' + settings.btfpos + '"]').length > 0) {
        tracker.error({
          "tl": "placementOcuppied"
        });
        return removers;
        return false;
      }*/

      if ($('div[data-pos="13"]').length > 0 && $('div[data-pos="25"]').length > 0) {
        tracker.error({
          "tl": "placementOcuppied"
        });
        return removers;
        return false;
      }

      helper_methods.crawl(settings.crawl).promise.then(function(d) {

        try {

          if ($('div[data-pos="13"]').length <= 0 && $('div[data-pos="25"]').length <= 0) {
            container.attr('data-pos', '13');
          } else if ($('div[data-pos="13"]').length > 0 && $('div[data-pos="25"]').length <= 0) {
            container.attr('data-pos', '25');
          }

          var prodsInpage = $('#productList>li[data-productid]').length;

          console.log(settings.custom);

          if (prodsInpage >= 14 && prodsInpage <= 20 && $('.sto-' + format).length < 1) {
            helper_methods.createFormat(container, tracker, settings.custom[0][2], d);
          } else if (prodsInpage > 20 && $('.sto-' + format).length < 2) {
            helper_methods.createFormat(container, tracker, settings.custom[0][2], d);
          } else {
            tracker.error({
              "tl": "notEnoughProdsInPage"
            });
          }

          console.log(container);

          style.use();

          // <editor-fold> INSERT FORMAT ********************************************************

          var insertFormat = function() {

            var widthTile = $('#productList>li').width();
            var widthLine = $('.product-list').width();
            var nbTile;

            if (widthTile * 2 > widthLine) {
              nbTile = 1;
            } else if (widthTile * 3 > widthLine) {
              nbTile = 2;
            } else {
              nbTile = 3;
            }

            var sponso = $('.hl-beacon-universal').length;
            if (sponso >= 1) {
              sponso = 1;
            }

            if ($('.sto-format').length == 0) {
              $('#productList>li').eq( nbTile * (5 + sponso)).before(container);
              $('.sto-format:first-of-type').attr('data-pos', 6 + sponso);
            } else if ($('.sto-format').length == 1) {
              $('#productList>li').eq( nbTile * (15 + sponso)).before(container);
              $('.sto-format:nth-of-type(2)').attr('data-pos', 16 + sponso);
            } else if ($('.sto-format').length == 2) {
              $('#productList>li').eq( nbTile * (25 + sponso)).before(container);
              $('.sto-format:nth-of-type(3)').attr('data-pos', 26 + sponso);
            }
          }

          insertFormat();


          // </editor-fold> *********************************************************************


          // <editor-fold> LEFT *****************************************************************

          // TEXTE
          if (left_txt && left_txt != "") {
            $(container).find('.sto-' + placeholder + '-left').prepend($("<p class='sto-" + placeholder + "-left-txt'>" + left_txt + "</p>"));
          }

          // </editor-fold> *********************************************************************

          tracker.display({
            "po": pos
          });

          $('.sto-' + placeholder + '-w-butterfly').attr('data-format-type', settings.format);
          $('.sto-' + placeholder + '-w-butterfly').attr('data-crea-id', settings.creaid);

          if (settings.text_cta != "") {
            $(".sto-" + placeholder + "-cta").append('<span>' + settings.text_cta + '</span>');
          }
          if (settings.claim_text != "") {
            $(".sto-" + placeholder + "-claim").append('<span>' + settings.claim_text + '</span>');
          }

          if (settings.redirect != "") {
            $(".sto-" + placeholder + "-cta").on('click', function() {
              try {
                tracker.click({
                  "tl": "cta",
                  "po": pos
                });
              } catch (e) {
                console.log(e)
              }
              window.open(settings.redirect, settings.target);
            });
          }

          var prodUrl = $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li>a').attr('href');
          $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-vignette').off().on('click', function() {
            tracker.moreInfo({
              "po": pos
            });
            window.open(prodUrl, '_self');
          });


          /*for (var i = 0; i < 1; i++) {
            browseObj(i, d, tracker);
          }*/

          var prodName = settings.custom[0][1];
          var prodPriceOld = $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-original-price') ? $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-original-price').text() : "";

          var prodPromo = $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.thumbnail span.pl-promo') ? $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.thumbnail span.pl-promo').text() : "";

          var prodPriceFinal = $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-reduced-price') ? $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-reduced-price').text() : "";

          if ($('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-eco-part').length > 0) {
            var priceEco = $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-eco-part').text();
          }else{
            var priceEco = '';
          }

          if ($('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-reduced-price').length > 0) {
            $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-eco-part').remove();
            var prodPriceFinal = $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-reduced-price').text();
          } else if ($('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-price').length > 0) {
            $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-eco-part').remove();
            var prodPriceFinal = $('.sto-' + placeholder + '-w-butterfly .sto-product-container>li').eq(0).find('.pl-price-line .pl-price').text();
          } else {
            var prodPriceFinal = "";
          }



          if (settings.custom[0][3] != null && settings.custom[0][3] != "None") {
            var prodRatings = settings.custom[0][3];
            prodRatings = prodRatings.replace('.', '_');
          }


          var prodRatingsDom = $('<span class="product-rating-stars"> <span class="disable-stars"> <span class="stars icon icon-star-disable"></span><span class="stars icon icon-star-disable"></span><span class="stars icon icon-star-disable"></span><span class="stars icon icon-star-disable"></span><span class="stars icon icon-star-disable"></span> </span> <span class="enable-stars rating-' + prodRatings + '"> <span class="stars icon icon-star-enable"></span><span class="stars icon icon-star-enable"></span><span class="stars icon icon-star-enable"></span><span class="stars icon icon-star-enable"></span><span class="stars icon icon-star-enable"></span> </span> </span>');



          var prodComment = settings.custom[0][4];
          $('.sto-' + placeholder + '-content').append('<div class="sto-' + placeholder + '-w-infos"></div>');

          $('.sto-' + placeholder + '-w-infos').append('<div class="sto-' + placeholder + '-infos-title">' + prodName + '</div><div class="sto-' + placeholder + '-infos-prices"><span class="sto-reduction">' + prodPromo + '</span> ' + prodPriceFinal + ' <span class="sto-old-price">' + prodPriceOld + '</span><span class="sto-eco-price">' + priceEco + '</span></div>');

          if (settings.custom[0][3] != null && settings.custom[0][3] != "None") {
            $('.sto-' + placeholder + '-w-butterfly').attr('data-type', 'ratings');
            $('.sto-' + placeholder + '-w-infos').append('<div class="sto-' + placeholder + '-infos-ratings"></div>');
            $('.sto-' + placeholder + '-infos-ratings').prepend(prodRatingsDom);
          }

          if (prodComment != "" && settings.custom[0][3] != null && settings.custom[0][3] != "None") {
            $('.sto-' + placeholder + '-w-infos').append('<div class="sto-' + placeholder + '-infos-comments">\"' + prodComment + '\"</div>');
          } else {
            $('.sto-' + placeholder + '-w-butterfly').attr('data-type', 'name');
          }

          /*if (settings.custom[0][3] != null && prodComment != "") {

          } else {
            $('.sto-' + placeholder + '-w-butterfly').attr('data-type', 'name');
          }*/

          //center content
          var centerContent = function() {
            var logoH = $(".sto-" + placeholder + "-w-logo").height();
            var contentH = $(".sto-" + placeholder + "-content").height();
            var selectionH = $(".sto-" + placeholder + "-selection").height();
            if ($(".sto-" + placeholder + "-w-cta")) {
              var recetteH = $(".sto-" + placeholder + "-w-cta").height();
            } else {
              var recetteH = 0;
            }
            var contentPos = (selectionH - (contentH + recetteH + logoH)) / 2;
            $(".sto-" + placeholder + "-content").css('margin-top', contentPos + 'px');
          };

          var btffHomote;
          window.addEventListener("resize", function() {

            var resizeFormat = function() {
                var widthTile = $('#productList>li').width();
                var widthLine = $('.product-list').width();
                var nbTile;

                if (widthTile * 2 > widthLine) {
                  nbTile = 1;
                } else if (widthTile * 3 > widthLine) {
                  nbTile = 2;
                } else {
                  nbTile = 3;
                }

                $('.sto-format').each(function() {
                  var dataPos = $(this).attr('data-pos');

                  if (window.matchMedia("(max-width: 767px)").matches) {
                    $('#productList>li:visible').eq( nbTile * (dataPos - 1)).before(this);
                  } else {
                    $('#productList>li').eq( nbTile * (dataPos - 1)).before(this);
                  }

                });
              }
              resizeFormat();

            //BTF height homotetique
            $('.sto-' + placeholder + '-w-butterfly').css('height', ((77.3 * $('.sto-' + placeholder + '-w-butterfly').width()) / 100) + 'px');
            //Logo height homotetique
            $('.sto-' + placeholder + '-cta,  .sto-' + placeholder + '-logo').css('height', ((25 * $('.sto-' + placeholder + '-logo').width()) / 100) + 'px');

            centerContent();

          });
          window.setTimeout(function() {
            //window.dispatchEvent(new window.Event("resize"));
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
          });

          // STORE FORMAT FOR FILTERS
          sto.globals["print_creatives"].push({
            html: $('.sto-' + placeholder + '-w-butterfly'),
            parent_container_id: "#productList",
            type: settings.format,
            crea_id: settings.creaid,
            position: pos
          });


        } catch (e) {
          console.log(e);
          style.unuse();
          $(".sto-" + placeholder + "-container").remove();
          tracker.error({
            "te": "onBuild-format",
            "tl": e,
            "po": pos
          });
        }
      }).then(null, console.log.bind(console));


      return removers;
    });

  }

  function verifAvailability(a, d) {
    var l = a[ctxt_prodlist].length;
    for (var i = 0; i <= (l - 1); i++) {
      var value = a[ctxt_prodlist][i];
      if (d[value] !== undefined && d[value].dispo !== false) {
        sto_global_products.push(value);
        sto_global_nbproducts += 1;
        return value;
      }
    }
    return false;
  }

  function addButton(cug, title, index) {
    $('.sto-' + placeholder + '-w-buttons').append('<button id="' + cug + '" data-index="' + (index + 1) + '">' + title + '</button>');
  }

  function browseObj(index, d, tracker) {
    var currentbObj = custom[index];
    if (currentbObj) {
      var title = currentbObj[ctxt_title],
        modal = currentbObj[ctxt_modal],
        available = verifAvailability(currentbObj, d);
      modal = modal === "true" ? true : false;

      if (available != false) {
        if (modal) {
          sto_global_modal = true;
        }
        addButton(available, title, index, tracker);
      }
    }

  }

  function firstDisplay() {
    $('.sto-' + placeholder + '-w-butterfly .sto-product-container .vignette_produit_info:first-child').show();
    $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).addClass('sto-' + placeholder + '-selButton');
    if (settings.custom_images === true) {
      $('.sto-' + placeholder + '-w-butterfly').attr('data-index', $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).attr('data-index'));
    }
  }

  function selectProduct(index) {


    $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button').removeClass('sto-' + placeholder + '-selButton');
    $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button[data-index="' + index + '"]').addClass('sto-' + placeholder + '-selButton');

    index = parseInt(index) - 1;

    $('.sto-' + placeholder + '-w-vignette').attr('data-ind', index);
    $('.sto-' + placeholder + '-w-vignette').attr('data-href', settings.custom[index][3]);

  }

} catch (e) {

}
