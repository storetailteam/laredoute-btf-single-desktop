var settings = require("./settings.json");
settings.btf_container_img = settings.btf_container_img === "" ? "none" : "url(./../../img/"+settings.btf_container_img+")";
settings.btf_container_img_mobile = settings.btf_container_img_mobile === "" ? "none" : "url(./../../img/"+settings.btf_container_img_mobile+")";
settings.btf_couleur = settings.btf_couleur === "" ? "transparent" : settings.btf_couleur;
settings.buttons_couleur = settings.buttons_couleur === "" ? "transparent" : settings.buttons_couleur;
settings.redirect_txt_color = settings.redirect_txt_color === "" ? "transparent" : settings.redirect_txt_color;
settings.title_bg_color = settings.title_bg_color === "" ? "transparent" : settings.title_bg_color;
settings.title_txt_color = settings.title_txt_color === "" ? "transparent" : settings.title_txt_color;
settings.btnRedirecBGColor = settings.btnRedirecBGColor === "" ? "transparent" : settings.btnRedirecBGColor;
settings.btnRedirecTextColor = settings.btnRedirecTextColor === "" ? "transparent" : settings.btnRedirecTextColor;
settings.buttons_couleur_select = settings.buttons_couleur_select === "" ? "transparent" : settings.buttons_couleur_select;
settings.buttons_img = settings.buttons_img === "" ? "none" : "url(./../../img/"+settings.buttons_img+")";
settings.buttons_img_select = settings.buttons_img_select === "" ? "none" : "url(./../../img/"+settings.buttons_img_select+")";
settings.buttons_txt = settings.buttons_txt === "" ? "transparent" : settings.buttons_txt;
settings.buttons_txt_select = settings.buttons_txt_select === "" ? "transparent" : settings.buttons_txt_select;
settings.download_img = settings.download_img === "" ? "none" : "url(./../../img/"+settings.download_img+")";
settings.border_color = settings.border_color === "" ? "#808080" : settings.border_color;
settings.buttons_width = settings.buttons_width === "" ? "none" : settings.buttons_width+"px";
settings.logo_img = settings.logo_img === "" ? "none" : "url(./../../img/"+settings.logo_img+")";
settings.claim_text_color = settings.claim_text_color === "" ? "transparent" : settings.claim_text_color;
settings.bg_carrousel = settings.bg_carrousel === "" ? "none" : "url(./../../img/"+settings.bg_carrousel+")";
settings.bg_big_carrousel = settings.bg_big_carrousel === "" ? "none" : "url(./../../img/"+settings.bg_big_carrousel+")";
settings.design_left = settings.design_left === "" ? "none" : "url(./../../img/"+settings.design_left+")";
settings.design_right = settings.design_right === "" ? "none" : "url(./../../img/"+settings.design_right+")";
settings.button_select_arrow = settings.button_select_arrow === "" ? "none" : "url(./../../img/"+settings.button_select_arrow+")";
settings.arrow_pos_button_select = settings.arrow_pos_button_select === "" ? "0" : settings.arrow_pos_button_select;
settings.sprite_retailer = settings.sprite_retailer === "" ? "none" : "url(./../../img/"+settings.sprite_retailer+")";
settings.text_legal_color = settings.text_legal_color === "" ? "transparent" : settings.text_legal_color;
settings.redirect_bg_color = settings.redirect_bg_color === "" ? "transparent" : settings.redirect_bg_color;
settings.btn_all_abk = settings.btn_all_abk === "" ? "none" : "url(./../../img/"+settings.btn_all_abk+")";
settings.container_img_01 = settings.container_img_01 === "" ? "none" : "url(./../../img/"+settings.container_img_01+")";
settings.container_img_02 = settings.container_img_02 === "" ? "none" : "url(./../../img/"+settings.container_img_02+")";
settings.container_img_03 = settings.container_img_03 === "" ? "none" : "url(./../../img/"+settings.container_img_03+")";
settings.container_img_04 = settings.container_img_04 === "" ? "none" : "url(./../../img/"+settings.container_img_04+")";
settings.container_img_05 = settings.container_img_05 === "" ? "none" : "url(./../../img/"+settings.container_img_05+")";
settings.logo_img_01 = settings.logo_img_01 === "" ? "none" : "url(./../../img/"+settings.logo_img_01+")";
settings.logo_img_02 = settings.logo_img_02 === "" ? "none" : "url(./../../img/"+settings.logo_img_02+")";
settings.logo_img_03 = settings.logo_img_03 === "" ? "none" : "url(./../../img/"+settings.logo_img_03+")";
settings.logo_img_04 = settings.logo_img_04 === "" ? "none" : "url(./../../img/"+settings.logo_img_04+")";
settings.logo_img_05 = settings.logo_img_05 === "" ? "none" : "url(./../../img/"+settings.logo_img_05+")";
settings.redirectBtnImg = settings.redirectBtnImg === "" ? "none" : "url(./../../img/"+settings.redirectBtnImg+")";
settings.buttons_police_size = settings.buttons_police_size === "" ? "12" : settings.buttons_police_size;
settings.widthRedirectBtn = settings.widthRedirectBtn === "" ? "80%" : settings.widthRedirectBtn;
settings.prod_img_01 = settings.prod_img_01 === "" ? "none" : "url(./../../img/"+settings.prod_img_01+")";
settings.prod_img_02 = settings.prod_img_02 === "" ? "none" : "url(./../../img/"+settings.prod_img_02+")";
settings.prod_img_03 = settings.prod_img_03 === "" ? "none" : "url(./../../img/"+settings.prod_img_03+")";
settings.left_bg_couleur = settings.left_bg_couleur === "" ? "transparent" : settings.left_bg_couleur;
settings.left_text_couleur = settings.left_text_couleur === "" ? "transparent" : settings.left_text_couleur;
settings.left_border_couleur = settings.left_border_couleur === "" ? "transparent" : settings.left_border_couleur;

module.exports = function (source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, `${settings.format}_${settings.name}_${settings.creaid}`)
        .replace(/__FORMAT__/g, settings.format)
        .replace(/__BTF_CONTAINE_IMG__/g,settings.btf_container_img)
        .replace(/__BTF_CONTAINE_IMG_MOB__/g,settings.btf_container_img_mobile)
        .replace(/__BTFCOLOR__/g,settings.btf_couleur)
        .replace(/__BUTTONCOULEUR__/g,settings.buttons_couleur)
        .replace(/__BUTTONCOULEURSELECT__/g,settings.buttons_couleur_select)
        .replace(/__BUTTONIMG__/g,settings.buttons_img)
        .replace(/__BUTTONIMGSELECT__/g,settings.buttons_img_select)
        .replace(/__BUTTONTXT__/g,settings.buttons_txt)
        .replace(/__BUTTONTXTSELECT__/g,settings.buttons_txt_select)
        .replace(/__BUTTONSELECTARROW__/g,settings.button_select_arrow)
        .replace(/__BORDERCOLOR__/g,settings.border_color)
        .replace(/__LOGOIMG__/g,settings.logo_img)
        .replace(/__CLAIMTXTCOLOR__/g,settings.claim_text_color)
        .replace(/__BUTTONSWIDTH__/g,settings.buttons_width)
        .replace(/__BGCARROUSEL__/g,settings.bg_carrousel)
        .replace(/__BGBIGCARROUSEL__/g,settings.bg_big_carrousel)
        .replace(/__DESIGNLEFT__/g,settings.design_left)
        .replace(/__DESIGNRIGHT__/g,settings.design_right)
        .replace(/__DL_IMG__/g,settings.download_img)
        .replace(/__APOSBTNSELECT__/g,settings.arrow_pos_button_select)
        .replace(/__SPRITERETAILER__/g,settings.sprite_retailer)
        .replace(/__REDIRTXTCOL__/g,settings.redirect_txt_color)
        .replace(/__REDIRBGCOL__/g,settings.redirect_bg_color)
        .replace(/__TITLBGCOLOR__/g,settings.title_bg_color)
        .replace(/__TITLTXTCOLOR__/g,settings.title_txt_color)
        .replace(/__BTNALLABK__/g,settings.btn_all_abk)
        .replace(/__LEGAL_TXT_COLOR__/g,settings.text_legal_color)
        .replace(/__BTN_POLICE_SIZE__/g,settings.buttons_police_size)
        .replace(/__BTF_BG_01__/g,settings.container_img_01)
        .replace(/__BTF_BG_02__/g,settings.container_img_02)
        .replace(/__BTF_BG_03__/g,settings.container_img_03)
        .replace(/__BTF_BG_04__/g,settings.container_img_04)
        .replace(/__BTF_BG_05__/g,settings.container_img_05)
        .replace(/__BTF_LOGO_01__/g,settings.logo_img_01)
        .replace(/__BTF_LOGO_02__/g,settings.logo_img_02)
        .replace(/__BTF_LOGO_03__/g,settings.logo_img_03)
        .replace(/__BTF_LOGO_04__/g,settings.logo_img_04)
        .replace(/__BTF_LOGO_05__/g,settings.logo_img_05)
        .replace(/__BTN_REDIRECT_IMG__/g,settings.redirectBtnImg)
        .replace(/__BTN_REDIRECT_BG_COLOR__/g,settings.btnRedirecBGColor)
        .replace(/__BTN_REDIRECT_TXT_COLOR__/g,settings.btnRedirecTextColor)
        .replace(/__BTN_SIZE__/g,settings.widthRedirectBtn)
        .replace(/__BTF_PROD_01__/g,settings.prod_img_01)
        .replace(/__BTF_PROD_02__/g,settings.prod_img_02)
        .replace(/__BTF_PROD_03__/g,settings.prod_img_03)
        .replace(/__LEFT_BGCOLOR__/g,settings.left_bg_couleur)
        .replace(/__LEFT_TEXT_COLOR__/g,settings.left_text_couleur)
        .replace(/__LEFT_BORDER_COLOR__/g,settings.left_border_couleur);
    return test;
};
